#-------------------------------------------------
#
# Project created by QtCreator 2013-08-03T20:13:28
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = ProdCons
TEMPLATE = app


SOURCES += main.cpp

HEADERS  += \
    producer.h \
    consumer.h \
    resources.h \
    mainwidget.h

FORMS    +=
