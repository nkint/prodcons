#ifndef RESOURCES_H
#define RESOURCES_H

#include <QDebug>
#include <QReadWriteLock>
#include <QWaitCondition>

class Resources {

public:

    Resources() {
        qDebug() << "Resources > ctor";
        lastFrame = 0;
    }

    ~Resources() {
        qDebug() << "Resources > dtor";
    }

    int lastFrame;
    QReadWriteLock lastFrame_read;

    QWaitCondition waitCondition;

};

#endif // RESOURCES_H
