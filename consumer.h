#ifndef CONSUMER_H
#define CONSUMER_H

#include <QDebug>
#include <QThread>
#include <QMutex>
#include <QWaitCondition>
#include <QReadWriteLock>
#include <QString>
#include "resources.h"

class Consumer : public QThread
{
    Q_OBJECT

public:
    Consumer(int id, Resources *r, QObject *parent=0):
        QThread(parent),
        abort(false)
    {
        this->r = r;
        this->id = id;
    }
    ~Consumer() {
        qDebug() << QString("Consumer %1 dtor").arg(id);
        stopConsume();
        wait();
    }

    void stopConsume() {
        abortMutex.lock();
        abort = true;
        abortMutex.unlock();
    }

protected:
    Resources *r;
    int id;

    int consumeMessage(int val)
    {
        qDebug() << QString("#%1 Consuming : %2").arg(id).arg(val);
        msleep(1000*id);
        emit newComputation(val);

        return val;
    }

    void run()
    {
        int n = -1;
        forever {
            r->lastFrame_read.lockForRead();
            if(r->lastFrame==-1 && r->lastFrame <= n) {
                qDebug() << QString("Consumer %1: going to sleep").arg(id);
                r->waitCondition.wait(&r->lastFrame_read);
                qDebug() << QString("Consumer %1: awake").arg(id);
            }
            n = r->lastFrame;
            r->lastFrame_read.unlock();

            consumeMessage(n);

            abortMutex.lock();
            if(abort) {
                abortMutex.unlock();
                break;
            } abortMutex.unlock();
        }
        emit finished();
    }

private:
    QMutex abortMutex;
    bool abort;

signals:
    void newComputation(int);
    void finished();
};

#endif // CONSUMER_H
