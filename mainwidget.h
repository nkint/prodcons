#ifndef MAINWIDGET_H
#define MAINWIDGET_H

#include <QWidget>
#include <QLabel>
#include <QCloseEvent>
#include <QKeyEvent>
#include <QVBoxLayout>
#include "resources.h"
#include "producer.h"
#include "consumer.h"

class MainWidget : public QWidget
{
    Q_OBJECT
    
public:
    //------------------------------------------------------------------ ctor dtor
    explicit MainWidget(QWidget *parent = 0) : QWidget(parent) {
        initGUI();

        initProducer();
        initConsumers();
    }

    ~MainWidget()
    {
    }

    //------------------------------------------------------------------ 1 prod, n cons
    void initProducer()
    {
        producer = new Producer(&r);
        connect(producer, SIGNAL(newFrame(int)),
                this, SLOT(showProducer(int)));
        connect(producer, SIGNAL(finished()),
                producer, SLOT(deleteLater()));

        producer->start();
    }

    void initConsumers()
    {
        consumerCounter = 0;

        consumer1 = new Consumer(1, &r);
        connect(consumer1, SIGNAL(newComputation(int)),
                this, SLOT(showConsumer1(int)));
        connect(consumer1, SIGNAL(finished()),
                consumer1, SLOT(deleteLater()));
        connect(consumer1, SIGNAL(destroyed()),
                this, SLOT(incrementConsumerCounter()));

        consumer2 = new Consumer(2, &r);
        connect(consumer2, SIGNAL(newComputation(int)),
                this, SLOT(showConsumer2(int)));
        connect(consumer2, SIGNAL(finished()),
                consumer2, SLOT(deleteLater()));
        connect(consumer2, SIGNAL(destroyed()),
                this, SLOT(incrementConsumerCounter()));

        consumer3 = new Consumer(3, &r);
        connect(consumer3, SIGNAL(newComputation(int)),
                this, SLOT(showConsumer3(int)));
        connect(consumer3, SIGNAL(finished()),
                consumer3, SLOT(deleteLater()));
        connect(consumer3, SIGNAL(destroyed()),
                this, SLOT(incrementConsumerCounter()));

        consumer1->start();
        consumer2->start();
        consumer3->start();
    }

    void stopProducer() {
        producer->stopProduce();
    }

    void stopConsumers() {
        consumer1->stopConsume();
        consumer2->stopConsume();
        consumer3->stopConsume();
    }

    void stopAll()
    {
        stopProducer();
        stopConsumers();
        qDebug() << "wake all!";
        r.waitCondition.wakeAll();
    }

    //------------------------------------------------------------------ gui
    void initGUI() {
        QVBoxLayout *l = new QVBoxLayout;
        this->producerLabel = new QLabel;
        l->addWidget(producerLabel);
        this->consumer1Label = new QLabel;
        l->addWidget(consumer1Label);
        this->consumer2Label = new QLabel;
        l->addWidget(consumer2Label);
        this->consumer3Label = new QLabel;
        l->addWidget(consumer3Label);
        this->setLayout(l);
    }

    void keyPressEvent(QKeyEvent* event) {
        if(event->key() == Qt::Key_S) {
            qDebug() << "pressed s, stop all";
            stopAll();
        }
        if(event->key() == Qt::Key_R) {
            qDebug() << "pressed r, restart all";
            initProducer();
            initConsumers();
        }
    }

    void closeEvent(QCloseEvent * event)
    {
        emit shutdown();
        this->close();
    }

private:
    QLabel *producerLabel;
    QLabel *consumer1Label;
    QLabel *consumer2Label;
    QLabel *consumer3Label;

    Resources r;
    Producer *producer;
    Consumer *consumer1;
    Consumer *consumer2;
    Consumer *consumer3;

    int consumerCounter;

private slots:
    void showProducer(int n) {
        this->producerLabel->setText(QString("Producer: %1").arg(n));
    }
    void showConsumer1(int n) {
        this->consumer1Label->setText(QString("Consumer1: %1").arg(n));
    }
    void showConsumer2(int n) {
        this->consumer2Label->setText(QString("Consumer2: %1").arg(n));
    }
    void showConsumer3(int n) {
        this->consumer3Label->setText(QString("Consumer3: %1").arg(n));
    }

    void incrementConsumerCounter() {
        consumerCounter++;
        if(consumerCounter==3) {
            qDebug() << "..... could restart!";
            initProducer();
            initConsumers();
        }
    }

signals:
    void shutdown();
};

#endif // MAINWIDGET_H
