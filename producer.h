#ifndef PRODUCER_H
#define PRODUCER_H

#include <QDebug>
#include <QThread>
#include <QMutex>
#include <QReadWriteLock>
#include <QWaitCondition>
#include "resources.h"

class Producer : public QThread
{
    Q_OBJECT
public:
    explicit Producer(Resources *r, QObject *parent = 0) :
        QThread(parent),
        abort(false)
    {
        qDebug() << "producer > ctor";
        frameCount = 0;
        this->r = r;
    }

    ~Producer() {
        qDebug() << "Producer dtor";
        stopProduce();
        wait();
    }

    void stopProduce() {
        abortMutex.lock();
        abort = true;
        abortMutex.unlock();
    }
    
protected:
    Resources *r;
    int frameCount;

    void produce()
    {
        frameCount++;
        r->lastFrame = frameCount;
        qDebug() << QString("Producing: %1").arg(frameCount);
        emit newFrame(frameCount);
    }

    void run()
    {
        forever {
            abortMutex.lock();
            if(abort) {
                abortMutex.unlock();
                break;
            } abortMutex.unlock();

            r->lastFrame_read.lockForWrite();
            produce();
            r->waitCondition.wakeAll();
            r->lastFrame_read.unlock();

            msleep(200);
        }
        emit finished();
    }

private:
    QMutex abortMutex;
    bool abort;

signals:
    void newFrame(int);

};

#endif // PRODUCER_H
